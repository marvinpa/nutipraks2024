// kommenteeri html-i korral (article), eemalda kommentaar pdf-i korral (book)
//:doctype: book
:toc: left
:toc-title: Sisukord
:figure-caption: Joonis
//:chapter-label: Peatükk
:chapter-signifier: Peatükk
:stem: latexmath
:sectnums:
:source-highlighter: coderay
//:source-highlighter: rouge
//:rouge-style: thankful_eyes
:stylesheet: ../dark-blue.css
:imagesdir:
//:source-language: c
:docinfo: private
:icons: font
// pole mõju html-i korral, kasulik pdf-i korral
:align-text: justify

= ESP-NOW praktikum
Nutilahenduste praktikum LOTI.05.060
07.05.2024

== Ülevaade

Selles praktikumis tutvud Espressif'i SoC'idel kasutatava ESP-NOW traadita kommunikatsiooniprotokolliga. ESP-NOW'd saab edukalt kasutada targa kodu, kaugjuhtimis- jne. rakendustes.

=== Vajalikud vahendid

. Internetiga arvuti
. DEV32-arendusplaadid
. Wemos D1 Mini-d
. Mitmsugused sensorid ja täiturid

=== Praktikumis esmakordselt kasutusele tulevate teemade tutvustused

==== ESP-NOW

*ESP-NOW* on traadita side protokoll, mis baseerub __data-link__-kihil, millega vähendatakse https://en.wikipedia.org/wiki/OSI_model[OSI mudeli^] kihtide arvu 5 võrra. Sellisel viisil ei pea andmeid suunama läbi __network__-kihi, __transport__-kihi, __session__-kihi, __presentation__-kihi ega __application__-kihi. Samuti pole vajadust iga kihi paketipäiste või lahtipakkijate järle, tänu millele saadakse kiire vastus, sest väheneb paketikaost põhjustatud viivis ülekoormatud võrkudes.

.ESP-NOW mudel
[#ESP-NOW,link=https://www.espressif.com/sites/all/themes/espressif/images/esp-now/model-en.png]
image::model-en.png[,1000,333,align="center"]

Protokoll töötab nii WiFi kui ka Bluetooth LE-ga ja teotab paljusid Espressif'i  SoC-e, mille on olemas WiFi. Andmevahetuse turvamiseks on ECDH- ja AES-algoritmid. Paaritumismeetod on kiire ja kasutajasõbralik, võimaldades ringhäälingut (__broadcast__) ühelt seadmelt paljudele (__one-to-many__), paljudelt ühele (__many-to-one__) ja dupleksandmevahetust. Kui paaritamine on tehtud, on ühendus __peer-to-peer__ (__P2P__) ja turvaline. Kui üks osapooltest peaks kaotama toite või tegema algkäivituse, siis ühendub see seade automaatselt teise osapoolega ja jätkab kommunikatsioonis osalemist.

*ESP-NOW omadusi:*
. krüpteeritud ja krüpteerimata __unicast__-side;
. segamini krüteeritud ja krüpteerimata osapooled;
. kuni 250-baidised andmepaketid;
. saatmise tagastusfunktsioon (__callback__), millega saab anda infot rakenduskihile (__application layer__) ülekande edukusest.

*ESP-NOW teadaolevad piirangud:*
. piiratud hulk krüpteeritud osapooli. Toetatud on kuni 10 krüpteeritud seadet __station__-re&#382;iimis, kuni 6 __SoftAP__- või __SoftAP__+__Station__-re&#382;iimis;
. kuigi toetatakse krüpteerimata osapooli, ei tohi selliste osapoolte arv olla üle 20, sisaldades ka krüptitud seadmeid;
. andmepakett ei tohi olla üle 250 baidi.

=== ESP-NOW kommunikatsiooniskeemid

. Simpleks
+
Simpleks-side korral saadetakse andmed ühelt seadmelt (__master__) teisele (__slave__) (joonis 2).
+
.ESP-NOW simpleks-side
[#ESP-NOW-simplex,link=https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2020/01/ESP_NOW_one_way_communication_two_boards.png?w=750&quality=100&strip=all&ssl=1]
image::ESP_NOW_one_way_communication_two_boards.png[align="center"]
. Ringhääling ühelt masterilt paljudele slave'idele
+
Ringhääling ehk __broadcasting__ ühelt __master__-seadmelt paljudele vastuvõtjatele (__slave__) ehk "__one-to-many__" võimaldab saata sama käsku kõigile seadmetele või erinevaid käske erinevatele seadmetele (joonis 3). Rakenduseks oleks näiteks kaugjuhtimispult.
+
.ESP-NOW __one-to-many__-side
[#ESP-NOW-one-to-many,link=https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2020/01/ESP_NOW_one_master_multiple_slaves.png?w=652&quality=100&strip=all&ssl=1]
image::ESP_NOW_one_master_multiple_slaves.png[align="center"]
. Ringhääling paljudelt __master__'itelt ühele __slave__'le
+
Sellises konfiguratsioonis (__many-to-one__) saab üks seade koguda andmeid paljudelt teistelt, mille küljes on andurid (joonis 4).
+
.ESP-NOW __many-to-one__-side
[#ESP-NOW-many-to-one,link=https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2020/01/ESP_NOW_one_slave_multiple_masters.png?w=649&quality=100&strip=all&ssl=1]
image::ESP_NOW_one_slave_multiple_masters.png[align="center"]
. ESP-NOW dupleks-side
+
ESP-NOW korral saab iga seade olla samaaegselt nii saatja kui ka vastuvõtja, võimaldades kahepoolset (dupleks)sidet. See võib toimuda ainult kahe seadme vahel (joonis 5) või võib seadmetest moodustada võrgu (joonis 6), milles kõik seadmed saavad kõigiga suhelda.
+
.ESP-NOW dupleks-side
[#ESP-NOW-duplex,link=https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2020/01/ESP_NOW_two_way_communication_two_boards.png?w=750&quality=100&strip=all&ssl=1]
image::ESP_NOW_two_way_communication_two_boards.png[align="center"]
+
.ESP-NOW võrk
[#ESP-NOW-network,link=https://i0.wp.com/randomnerdtutorials.com/wp-content/uploads/2020/01/ESP_NOW_multiple_boards_two_way_communication.png?w=585&quality=100&strip=all&ssl=1]
image::ESP_NOW_multiple_boards_two_way_communication.png[align="center"]

=== ESP-NOW programmeerimine

.ESP-NOW funktsioonid
[cols="1,2",frame=none,grid=none]
|===
| Funktsioon | Kirjeldus

| https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_now.html#_CPPv412esp_now_initv[esp_now_init()^] | Initsialiseerib ESP-NOW. WiFi tuleb initsialiseerida enne ESP-NOW-d

| https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_now.html#_CPPv416esp_now_add_peerPK19esp_now_peer_info_t[esp_now_add_peer()^] | Funktsioon seadmete paaritamiseks

| https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_now.html#_CPPv412esp_now_sendPK7uint8_tPK7uint8_t6size_t[esp_now_send()^] | Andmete saatmine

| https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_now.html#_CPPv424esp_now_register_send_cb17esp_now_send_cb_t[esp_now_register_send_cb()^] | Tagastusfunktsiooni registreerimine saatmise õnnestumise või läbikukkumise kohta

| https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/esp_now.html#_CPPv424esp_now_register_recv_cb17esp_now_recv_cb_t[esp_now_register_rcv_cb()^] | Tagastusfunktsiooni registreerimine andmete vastuvõtmiseks
|=== 

Saatja ja vastuvõtja programmeerimise üldskeemid oleksid järgmised:

*saatja*:

. initsialiseeri ESP-NOW;
. registreeri saatmise tagastusfunktsioon, et saada teada saatmise õnnestumisest;
. lisa teine osapool (vastuvõtja). Selleks tuleb teada vastuvõtja MAC-aadressi;
. saada andmed teisele osapoolele;

*vastuvõtja*:

. initsialiseeri ESP-NOW;
. registreeri vastuvõtu tagastusfunktsioon, mis kutsutakse välja, kui andmed saabuvad;
. tagastusfunktsioonis salvesta andmed globaalsesse muutujasse, et neid põhiprogrammis kasutada.

[NOTE]
====
Tähele tuleks panna, et ESP32 ja ESP8266 (Wemos D1 Mini) koodid erinevad veidi, sest WiFi-teegid on erinevad.
====

[IMPORTANT]
====
Pärast WiFi-re&#382;iimi seadmist tuleks kõik seadmed sättida samale WiFi kanalile:
====
[source,c++]
----
  WiFi.mode(WIFI_STA);
  wifi_set_channel(kanal);
----
Enne tuleks muidugi välja selgitada, millist WiFi kanalit seadmed kasutavad.

[CAUTION]
====
Seadmete vahel vahetatava andmepaketi struktuuris ei ole soovitatav kasutada tüübina *__String__*'i. Selle asemel tuleks kasutada tüübina *__char[]__*'i.
====

== Ülesanded

=== Ülesanne: ESP-NOW __one-to-many__

==== Kirjeldus

Käesolevas ülesandes tuleb realiseerida ESP-NOW __broadcast__ ühelt saatjalt mitmele vastuvõtjale (3-4). Saatjate ja vastuvõtjatena kasutada DEV32-plaate ja/või Wemos D1 Mini'sid.

[TIP]
====
Lisainfot pakub https://randomnerdtutorials.com/esp-now-one-to-many-esp32-esp8266/[randomnerdtutorials^].
====

==== Arvestus

Käesoleva ülesande arvestuse saamiseks:

. loo vastuvõtja kood, mis paneb saadud käsu peale DEV32-plaadi või Wemose sisseehitatud LED-i põlema, kustutab ära või paneb vilkuma;
. loo saatja kood, mis võimaldaks saata terminalist või nt. klahvistikult sisestatud:
.. sama käsku kindlate MAC-aadressidega seadmetele;
.. sama käsku kõikidele vastuvõtmiseks seadistatud seadmetele;
.. erinevat käsku erinevatele seadmetele;
. näita eelpool toodud konfiguratsioonide toimimine juhendajale ette.

=== Ülesanne: ESP-NOW __many-to-one__

==== Kirjeldus

Käesolevas ülesandes tuleb realiseerida ESP-NOW-d kasutades olukord, milles üks seade kogub teistelt seadmetelt (3-4) andmeid. Andmeid saatvatel seadmetel on erinevad ja/või ühesugused andurid. Vastuvõetud andmed kuvada vabalt valitud ekraanil.

[TIP]
====
Lisainfot pakub https://randomnerdtutorials.com/esp-now-many-to-one-esp32/[randomnerdtutorials^].
====

==== Arvestus

Käesoleva ülesande arvestuse saamiseks:

. loo saatjate koodid andmete kogumiseks ja vastuvõtjale edastamiseks;
. loo vastuvõtja kood saatjatelt andmete vastuvõtmiseks ja vabalt valitud ekraanil kuvamiseks;
. näita loodud süsteemi toimimine juhendajale ette.

=== Ülesanne: ESP-NOW dupleks

==== Kirjeldus

Käesolevas ülesandes tuleb realiseerida ESP-NOW-d kasutades kahepoolne side mitmete seadmete (kuni 4) vahel. Kõik seadmed peavad olema võimelised enda infot teistele edasi saatma ja kõigi teiste poolt saadetud infot vastu võtma ja selle alusel tegutsema, nt. seda kuvama.

[TIP]
====
Lisainfot pakub https://randomnerdtutorials.com/esp-now-two-way-communication-esp32/[randomnerdtutorials^].
====

==== Arvestus

Käesoleva ülesande arvestuse saamiseks:

. moodusta ESP-NOW süsteem DEV32-plaatidest ja/või Wemos D1 Minidest, kus süsteemi lõpp on praktikumiruumis, algus aga TÜTI fuajees selliselt , et süsteemi otstes olevad seadmed üksteist üle ESP-NOW ei näe;
. lisa vahepeale mõned DEV32-plaadid või Wemos D1 Mini-d, mis aitaksid omavahel ühendada süsteemi otsad;
. süsteemi algus saadab vabalt valitud sensori infot süsteemi lõppu;
. süsteemi lõpp kuvab konkreetselt temale saadetud infot;
. vahepealsed seadmed vahendavad süsteemi algusest saadetud infot edasi lõpu poole, lisades omalt poolt lõpu jaoks enda küljes olevate andurite info;
.. naaberseadmete infot ei tohi vahendada, kui see pole mõeldud edasisaatmiseks süsteemi lõppu;
//. loo DEV32-plaatidele ja/või Wemos D1 Mini-dele saatja-vastuvõtja koodid:
//. 
//.. seadmega ühendatud sensorite info või terminalist või klahvistikult või muul viisil antud käskude edastamiseks kõigile teistele seadmetele kas üldiselt või kõigile seadmele __broadcast__'ituna, kuid konkreetsele seadmele suunatult;
//.. teistelt seadmetelt vastuvõetud infot visualiseerimiseks või selle alusel tegutsemiseks, kui see info on adresseeritud kõigile seadmetele üldiselt või konkreetselt antud seadmele;
. näita loodud süsteemi toimimine juhendajale ette.

