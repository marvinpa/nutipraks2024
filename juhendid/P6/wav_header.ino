typedef struct __attribute__ ((__packed__)) {
    // RIFF Header
    char riff_header[4]; // Contains "RIFF"
    uint32_t wav_size; // Size of the wav portion of the file, which follows the first 8 bytes. File size - 8
    char wave_header[4]; // Contains "WAVE"
    
    // Format Header
    char fmt_header[4]; // Contains "fmt " (includes trailing space)
    uint32_t fmt_chunk_size; // Should be 16 for PCM
    uint16_t audio_format; // Should be 1 for PCM. 3 for IEEE Float
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate; // Number of bytes per second. sample_rate * num_channels * Bytes Per Sample
    uint16_t sample_alignment; // num_channels * Bytes Per Sample
    uint16_t bit_depth; // Number of bits per sample
    
    // Data
    char data_header[4]; // Contains "data"
    uint32_t data_bytes; // Number of bytes in data. Number of samples * num_channels * sample byte size
    // uint8_t bytes[]; // Remainder of wave file is bytes
} wav_header_t;

void print_wav_header(wav_header_t *h){
  Serial.println("WAV header");
  Serial.println();
  Serial.printf("riff header: %.4s\n", h->riff_header);
  Serial.printf("wav size: %ld bytes\n", h->wav_size);
  Serial.printf("wave header: %.4s\n", h->wave_header);
  Serial.printf("fmt header: %.4s\n", h->fmt_header);
  Serial.printf("fmt chunk size: %ld bytes\n", h->fmt_chunk_size);

  Serial.print("audio format: ");
  switch (h->audio_format){
    case 1:
      Serial.print("PCM");
      break;
    case 3:
      Serial.print("IEEE float");
      break;
    default:
      Serial.print("Unknown");
      break;
  }
  Serial.printf(" (%d)\n", h->audio_format);

  Serial.printf("channels: %d\n", h->num_channels);
  Serial.printf("sample rate: %ld\n", h->sample_rate);
  Serial.printf("byte rate: %ld\n", h->byte_rate);
  Serial.printf("sample alignment: %d\n", h->sample_alignment);
  Serial.printf("bit depth: %d\n", h->bit_depth);
  Serial.printf("data header: %.4s\n", h->data_header);
  Serial.printf("data size: %ld bytes\n", h->data_bytes);

  Serial.println(); 
}

ext::File wav_file;
void setup(){
    //
    // ... SD setup
    //
    wav_file = SD.open("/audio.wav", FILE_READ);

    wav_header_t header;
    wav_file.read((uint8_t *)&header, 44); // Reads first 44 bytes of header from the file
    print_wav_header(&header);
}