// kommenteeri html-i korral (article), eemalda kommentaar pdf-i korral (book)
//:doctype: book
:toc: left
//:toc-title: Sisukord
//:figure-caption: Joonis
//:chapter-label: Part
:chapter-signifier: Part
:stem: latexmath
:sectnums:
:source-highlighter: coderay
//:source-highlighter: rouge
//:rouge-style: thankful_eyes
:stylesheet: ../dark.css
:imagesdir:
//:source-language: c
:docinfo: private
:icons: font
// pole mõju html-i korral, kasulik pdf-i korral
:align-text: justify

= I2C Lab
Smart Solutions LOTI.05.060
07.03.2024

== Overview
//[.text-justify]
In this lab we will use different _I2C_ devices.

=== Resources You Need

. DEV32 development board
. 16x2 LCD
. 128x32 or 128x64 OLED
. Time of Flight sensor VL53L0X


=== Introductions to the Topics That Will be Used for the First Time in the Lab

==== I2C

//[.text-justify]
*I2C (I^2^C, IIC, Inter-Integrated Circuit)* is a synchronous half-duplex serial interface that uses packet switching for communication. In Atmel processors, this interface is called __TWI__, and in the Arduino IDE it is called __Wire__. It is used to communicate with peripherals that require low bandwidth (usually a maximum of 400 kbit/s) over short lines. I2C uses two bidirectional open collector/gate lines: *SDA* for the data and *SCL* for the clock signal, which is normally pulled high with a pullup resistor.

.Transistor with open collector inside each I2C device
image::open_collector_bjt.png[asi, 400,align=center]

//[.text-justify]
With this scheme, any device connected to the I2C bus can control the SDA and SCL lines. Since I2C allows multiple devices to be connected to the same data bus, it is necessary to use device addressing. The number of devices that can be connected to one bus is limited by the available address space, which is usually 7 bits wide and the total electrical capacity of the bus must be less than 400pF. The parties to the communication are the controller and the node, also called the master and the slave. The controller generates a clock and initiates communication with the nodes by sending a START signal, which means that the SDA is pulled low while the SCL is high. The controller then sends the address of the node with which it wants to start the conversation with and the READ/WRITE bit. After each message, the controller waits for a response from the client about the information sent, i.e. ACK (node pulls down SDA) or NACK (does not pull SDA down). An ACK response to the address message indicates that addressed device exists on the bus. The conversation then continues depending on whether the controller wanted to read or write. If you want to write, you can do so until all the desired data is sent or the node sends a NACK signal. Reading is similar - the node continues to write data sequentially while the controller responds with an ACK signal. Sending will stop after the controller responds with NACK. Communication with the device goes on until the STOP signal, which is the rising front on the SDA bus while the SCL is high. For a better overview with examples, it is recommended to refer to the I2C chapter of the RTC on the DEV32 https://datasheets.maximintegrated.com/en/ds/DS3231M.pdf[data sheet^] from p. 16.

==== RTC

//[.text-justify]
*RTC (Real-Time Clock)*  is a device that measures time very accurately. The DEV32 board has a https://datasheets.maximintegrated.com/en/ds/DS3231M.pdf[DS3231M^] real-time clock connected to the I2C bus. It can read time with an accuracy of ± 0.432 seconds per 24 h and store information about seconds, minutes, hours and date, including years and leap years. It also includes a temperature sensor.

=== LCD

//[.text-justify]
*LCD (Liquid-Crystal Display)* consists of transparent electrodes, liquid crystal and two polarizing filters. The liquid crystals are located between glass plates, each of which has transparent electrodes and polarizing filters. The polarization planes of polarizing filters are perpendicular to each other. The inner surfaces of the glass plates have parallel grooves, which are also perpendicular to each other on both plates. When the liquid crystal is in an unstressed state, the liquid crystals align with the grooves on the inner surface of the glass plate. Because they are perpendicular to the opposite plates, the liquid crystals between the plates must eventually make a 90-degree turn. The light that has passed through the first polarizer is polarized along the crystals aligned with the grooves of the plate, and following them, the light passes through the gap between the plates, as a result of which its polarization plane is also turned 90 degrees, and since the second polarizer is perpendicular to the first, the light passes through it unimpeded. When an electric field is applied to the crystals, the crystals begin to aling along electric field, starting to rotate the polarization plane of light less and less in proportion to the voltage, until the polarization plane no longer changes when passing through the crystal. Therefore, all the light is absorbed in the second filter and nothing comes through. The LCD does not produce its own light. Passive LCDs use the reflection of incident light. In order to increase contrast and visibility even in little or no external light, a backlight is used, which comes is mostly form LEDs.

//[.text-justify]
The LCD in the lab uses a parallel interface, but various modules are available that convert the LCD parallel interface to either an I2C or SPI serial interface. We have an I2C module that uses a PCF8574 chip.

=== ToF

//[.text-justify]
*Time of Flight (ToF)* or distance sensor measures the distance using a laser to measure the time it takes for light to return to the sensor from the surface which distance has being measured. ToF sensors are a key components in LIDAR systems, where measurements are made in many spatial directions. By knowing the sequence and direction of measurements, a model can be constructed from the points. The sensor used in the lab can measure distances up to 2 m using a laser with a wavelength of 940 nm.

== Tasks

=== Task: Finding the I2C Device Address

==== Description

//[.text-justify]
The first step in communicating to each I2C device is to find its address, which can be done by several ways. Once the device or module is connected to the processor, it is possible to use a program that finds all the I2C devices connected to the bus. In the case of a widely used devices and modules, its address can be found in the data sheets or elsewhere on the Internet. However, the data sheet of the device should be preferred. For example, searching the Internet for the I2C address of an MPU6050 gyroscope returns 0x68. However, on our DEV32 development board, the I2C scanner finds that there is a device at address 0x68, even if the MPU6050 is not connected. When the gyroscope is connected, its address is 0x69 instead. This is caused by the RTC on the board, the I2C address of which cannot be changed, but the MPU6050 module has an I2C address that can be changed by changing the status of one pin.

==== Assessment

For the assessment of the present task, you have to:

//[.text-justify]
. Show the instructor the program which in its __setup()__ part finds all the addresses of the connected I2C devices and outputs them.
. The program has to be easily integrated into other programs.

=== Reading the DS3231M Register

==== Description

//[.text-justify]
Unlike an UART connection, I2C does not use separate I/O registers. I2C itself modifies the registers and reads directly from the registers that the device can access to store settings or values. This allows devices using the I2C bus to avoid the need for complex firmware to change settings, for example AT commands.

//[.text-justify]
For many devices that use the I2C bus, there are libraries that do all the work in the background. You will use them in later tasks, but in this task you should start exchanging data and read the contents of the temperature registers from the DS3231M RTC according to the examples in the data sheet. Use the https://www.arduino.cc/reference/en/language/functions/communication/wire/[__Wire.h__ library^] to communicate. Using libraries that do I2C communications in the backround, is not allowed in this task.

//[.text-justify]
Commands required to write to a register are:
[cols="a,a",frame=none,grid=none]
|===
|
. https://www.arduino.cc/reference/en/language/functions/communication/wire/begintransmission/[beginTransmission()^]
. https://www.arduino.cc/reference/en/language/functions/communication/wire/write/[write()^] 
. https://www.arduino.cc/reference/en/language/functions/communication/wire/endtransmission/[endTransmission()^]
|
[none]
* addresses the I2C device, starts the writting operation
* adds a message to the communcation buffer
* sends the messages to the I2C device from the communcation buffer
|===

//[.text-justify]
Commands required to read from a register are:
[cols="a,a",frame=none,grid=none]
|===
|
. https://www.arduino.cc/reference/en/language/functions/communication/wire/requestfrom/[requestFrom()^]
. https://www.arduino.cc/reference/en/language/functions/communication/wire/read/[read()^]
|
[none]
* addresses the I2C device, starts the reading operation
* reads a message from the communication buffer
|===


==== Assessment

For the assessment of the present task, you have to:

//[.text-justify]
. Show the instructor the I2C address of the DS3231M from its data sheet.
. Show the instructor in which register you can read temperature value of the chip form the DS3231M data sheet.
. Show the instructor a program which reads the temperature form the registers and displays it on the __Serial Monitor__ in Celsius - it should be able to display both positive and negative values. Ask the instructor for the tool to create negative temperatures.

//[.text-justify]
[TIP]
====
Use the Internet to get information. The library itself cannot be used in this task, but it can be very useful to study its source code.
====

=== LCD and ToF Sensor

==== Description

//[.text-justify]
For this task, add devices to the setup of the previous task so that all devices work together at the same time.

//[.text-justify]
In present task, it is recommended to use libraries on your choice to communicate with the LCD and VL53L0X ToF sensor.

==== Assessment

For the assessment of the present task, you have to:

//[.text-justify]
. Show the instructor a program that periodically measures the distance with a ToF sensor and displays it on the LCD screen with the unit:
.. The display can not flicker.
.. The measurements must be averaged so that the output is stable.
.. Redirect the temperature values read from the DS323M to the LCD instead of the _Serial Monitor_, use the second row of the display.

=== Task: OLED

//[.text-justify]
For this task, add the devices to the setup of the previous two tasks so that all the devices work together at the same time.

//[.text-justify]
In this task, it is recommended to use libraries on your choice to communicate with the OLED. 

==== Assessment 
For the assessment of the present task, you have to:

//[.text-justify]
. Show the instructor a program that reads the input from the _Serial Monitor_ and displays the received message on the OLED
. The program must be able for word wrap in the case of multi-line messages

