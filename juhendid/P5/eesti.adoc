// kommenteeri html-i korral (article), eemalda kommentaar pdf-i korral (book)
//:doctype: book
:toc: left
:toc-title: Sisukord
:figure-caption: Joonis
//:chapter-label: Peatükk
:chapter-signifier: Peatükk
:stem: latexmath
:sectnums:
:source-highlighter: coderay
//:source-highlighter: rouge
//:rouge-style: thankful_eyes
:stylesheet: ../dark.css
:imagesdir:
//:source-language: c
:docinfo: private
:icons: font
// pole mõju html-i korral, kasulik pdf-i korral
:align-text: justify

= I2C-praktikum
Nutilahenduste praktikum LOTI.05.060
07.03.2024

== Ülevaade
//[.text-justify]
Käesolevas praktikumis õpid kasutama erinevaid __I2C__-seadmeid.

=== Vajalikud vahendid

. DEV32-arendusplaat
. 16x2 LCD-ekraan
. 128x32 või 128x64 OLED-ekraan
. Laserkaugussensor VL53L0X

=== Praktikumis esmakordselt kasutusele tulevate teemade tutvustused

==== I2C
//[.text-justify]
*I2C (I^2^C, IIC, __Inter-Integrated Circuit__)* on sünkroonne pool-dupleks jadaliides, mis kasutab suhtlemiseks pakettkommutatsiooni (__packet switching__). Atmeli protsessorites kutsutakse seda liidest nimega __TWI__  ja Arduino IDE-s __Wire__.  Seda kasutatakse väikest läbilaskevõimet (enamasti maksimaalselt 400 kb/s) vajavate perifeeriaseadmetega suhtlemiseks üle lühikeste liinide. I2C kasutab kahte kahesuunalist avatud kollektori/paisuga liini: *SDA* andmete jaoks ja *SCL* taktsignaali jaoks, mis on tavaolekus __pullup__ takistiga kõrgeks tõmmatud.

.Avatud kollektoriga transistor iga I2C-seadme sees
image::open_collector_bjt.png[asi, 400,align=center]

[.text-justify]
Sellise skeemiga saab iga I2C siini külge ühendatud seade kontrollida SDA- ja SCL-liine. Kuna I2C võimaldab ühendada mitut seadet sama andmesiini külge, siis on vaja kasutada seadmete adresseerimist. Ühe siini külge ühendatavate seadmete hulka piirab kasutada olev aadressiruum, mis on enamasti 7 bitti lai ja siini kogumahtuvus, mis peab jääma alla 400 pF. Suhtluse osapooled on kontroller ja sõlm, mida nimetatakse ka vastavalt ülemaks (__master__) ja alluvaks (__slave__). Kontroller genereerib takti ja alustab sõlmedega suhtlust, saates START-signaali, mis tähendab, et SDA viiakse madalaks sel ajal, kui SCL on kõrge. Seejärel saadab kontroller selle sõlme aadressi, millega ta vestlust tahab alustada ja READ/WRITE-biti. Peale igat sõnumit ootab kontroller kliendilt vastust saadetud info kohta, ehk ACK (sõlm tõmbab SDA madalaks) või NACK (ei tõmba madalaks). Aadressile oodatakse vastust ACK näitamaks, et selline seade on siinil olemas. Seejärel jätkub vestlus vastavalt sellele, kas kontroller soovis lugeda või kirjutada. Kui soov on kirjutada, siis võib seda teha seni, kuni kõik soovitud andmed saavad saadetud või sõlm saadab NACK-signaali. Samuti on lugemisega - sõlm jätkab järjest andmete kirjutamist, kuni kontroller vastab ACK-sigaaliga, esimese NACK-i peale saatmine lõpetatakse. Suhtlus ühe seadmega kestab kuni STOP-signaalini, milleks on tõusev front SDA-siinil sel ajal kui SCL on kõrge. Sellest parema ülevaate saamiseks koos näidetega on soovitav vaadata DEV32-plaadil oleva RTC https://datasheets.maximintegrated.com/en/ds/DS3231M.pdf[andmelehe^] I2C-peatükkiga alates lk. 16.

==== RTC

//[.text-justify]
*RTC (__Real-Time Clock__)* ehk *reaalajakell* on seade, mis mõõdab väga täpselt aega. DEV32-plaadil on https://datasheets.maximintegrated.com/en/ds/DS3231M.pdf[DS3231M-reaalajakell^], mis on ühendatud I2C-siini külge. See suudab lugeda aega täpsusega kuni ±0.432 sekundit 24 h kohta ja hoida infot sekundite, minutite, tundide ja kuupäeva kohta, kaasa arvatud aastad ja liigaastad. Samuti sisaldab ta endas temperatuuriandurit.

==== LCD

//[.text-justify]
*LCD (__Liquid-Crystal Display__)* ehk *vedelkristallekraan* koosneb läbipaistvatest elektroodidest, vedelkristallist ja kahest polariseerivast filtrist. Vedelkristallid asuvad klaasplaatide vahel, millel mõlemal on läbipaistavad elektroodid ja polariseerivad filtrid. Polariseerivate filtrite polarisatsioonitasandid on omavahel risti. Klaasplaatide sisepindadel on rööpvaod, mis on samuti mõlemal plaadil teiseteisega risti. Kui vedelkristall on pingestamata olekus, siis joonduvad vedelkristallid klaasplaadi sisepinnal olevate vagude järgi. Et need on vastasplaatidel risti, siis peavad vedelkristallid plaatide vahel tegema lõpuks 90-kraadise pöörde. Esimese polarisaatori läbinud valgus polariseeritakse pikki plaadi vagudega joondunud kristalle ja neid järgides läbib valgus plaatide vahe, mille tulemusena pööratakse tema polarisatsioonitasand samuti 90 kraadi ja kuna teine polarisaaator on esimesega risti, siis läbib valgus selle takistamatult. Kristallidele elektrivälja rakendamisel pingestades hakkavad kristallid pöörduma pikki elektrivälja, hakates pingega proportsionaalselt valguse polarisatsioonitasandit üha vähem pöörama, kuni polarisatsioonitasand enam kristalli läbides ei muutu. Sestap neeldub kogu valgus teises filtris ja läbi ei tule enam midagi. LCD ei tooda ise valgust. Passiivsetes LCD-des kasutatakse ära pealelangeva valguse peegeldumist. Et suurendada kontrasti ja nähtavust ka väheses või puuduvas välisvalguses, kasutatakse taustavalgust, milleks on enamasti LED-id.

//[.text-justify]
Praktikumis kasutatava tagantvalgustusega 16x2 LCD (16 veergu ja 2 rida sümboleid) loomuomane liides on paralleelliides, kuid erinevate moodulitega saab muuta LCD paralleelliidese kas I2C- või SPI-jadaliideseks. Meie kasutamegi I2C-moodulit, mis põhineb PCF8574-kiibil.

==== Laserkaugusandur

//[.text-justify]
*ToF (__Time of Flight__)* ehk laserkaugussensor määrab aja, mis kulub lasekiire tagasijõudmiseks sensorile pärast objektil peegeldumist ja arvutab selle järgi kauguse objektini. ToF-sensorid on peamine komponent LIDAR-süsteemides, kus tehakse mõõtmisi paljudes ruumisuundades. Teades mõõtmiste järjekorda ja suunda, saab punktidest konstrueerida mudeli. Praktikumis kasutusel olev VL53L0X-sensor suudab mõõta kuni 2 m kaugusi, kasutades 940 nm lainepikkusega laserit.

== Ülesanded

=== Ülesanne: I2C-seadme aadressi leidmine

==== Kirjeldus

//[.text-justify]
Esimene samm iga I2C-seadmega suhtlemise juures on selle seadme aadressi leidmine, milleks on mitu võimalust. Kui seade või moodul on ühendatud protsessori külge, on võimalik kasutada programmi, mis leiab kõik siini külge ühendatud I2C-seadmed. Kui tegemist on laialdaselt kasutatava seadme või mooduliga, siis võib selle aadressi leida andmelehtdelt või mujalt internetivarusest. Siiski tuleks eelistada seadme andmelehte. Otsides näiteks internetist MPU6050-güroskoobi I2C-aadressi, saadakse vastuseks 0x68. Samas leiab meie DEV32 arendusplaadil I2C skänner, et aadressil 0x68 on seade, isegi kui MPU6050 ei ole ühendatud. Kui güroskoop ühendada, siis on tema aadressiks hoopis 0x69. Seda põhjustab plaadil olev RTC, mille I2C aadressi ei ole võimalik muuta, kuid MPU6050 moodulil on I2C aadress kontrollitav ühe viigu olekut muutes. 

==== Arvestus

Käesoleva ülesande arvestuse saamiseks tuleb:

//[.text-justify]
. näidata juhendajale programmi, mis __setup()__-osas leiab kõik ühendatud I2C seadmete aadressid ja väljastab need;
. programm peaks olema võimalikult lihtsalt teistesse programmidesse integreeritav.

=== Ülesanne: DS3231M registri lugemine

//[.text-justify]
Erinevalt UART-ühendusest ei kasuta I2C eraldi sisend-väljundregistreid. I2C muudab ise registreid ja loeb otse registritest, millele seade ligi pääseb, kus hoitakse seadistust või väärtusi. See võimaldab I2C-siini kasutavatel seadmelel vältida vajadust keeruka püsivara (__firmware__) järgi, et seadistust muuta näiteks AT-käske kasutades.

//[.text-justify]
Paljude I2C-siini kasutavate seadmete jaoks on olemas teegid, mis teevad kogu töö peidetult ära. Neid kasutad hilisemates ülesannetes, kuid käesolevas ülesandes tuleks andmelehes toodud näidete järgi alustada andmevahetust ja lugeda DS3231M RTC-lt temperatuuriregistrite sisu. Suhtluseks kasutada https://www.arduino.cc/reference/en/language/functions/communication/wire/[__Wire.h__-teeki^].  Käesolevas ülesandes ei tohi kasutada ühtegi teeki, mis I2C-vestluse peidetult ära teeb.

//[.text-justify]
Registrisse kirjutamiseks vajalikud käsud on:
[cols="2a,2a",frame=none,grid=none]
|===
|
. https://www.arduino.cc/reference/en/language/functions/communication/wire/begintransmission/[beginTransmission()^]
. https://www.arduino.cc/reference/en/language/functions/communication/wire/write/[write()^] 
. https://www.arduino.cc/reference/en/language/functions/communication/wire/endtransmission/[endTransmission()^]
|
[none]
* adresseerib I2C-seadme, alustab kirjutamisoperatsiooni
* lisab kirjutatava sõnumi sidepuhvrisse
* saadab puhvrisse salvestatud sõnumid I2C-seadmesse
|===

//[.text-justify]
Registrist lugemiseks vajalikud käsud on
[cols="2a,2a",frame=none,grid=none]
|===
|
. https://www.arduino.cc/reference/en/language/functions/communication/wire/requestfrom/[requestFrom()^]
. https://www.arduino.cc/reference/en/language/functions/communication/wire/read/[read()^]
|
[none]
* adresseerib I2C-seadme, alustab lugemisoperatsiooni
* loeb sidepuhvrist sõnumi
|===

==== Arvestus

Käesoleva ülesande arvestuse saamiseks tuleb näidata juhendajale:

//[.text-justify]
. DS3231M andmelehest, mis on selle I2C-aadress ja kust selle leiab;
. DS3231M andmelehest, millisest registrist saab lugeda välja kiibi temperatuuri;
. programm, mis loeb registritest temperatuuri ja väljastab selle __Serial Monitorile__ Celsiuse skaalal - väljastada tuleb nii positiivseid kui ka negatiivseid temperatuure. Negatiivse temperatuuri saamiseks annab vahendi juhendaja.

//[.text-justify]
[TIP]
====
Info saamiseks kasutada internetti. Käesolevas ülesandes ei või kasutada teeki ennast, kuid selle lähtekoodi uurimisest võib palju kasu olla.
====

=== Ülesanne: LCD-ekraan ja ToF-sensor

==== Kirjeldus

//[.text-justify]
Käesoleva ülesande korral lisada seadmed eelmise ülesande seadistusele, et kõik seadmed töötaks üheaegselt koos.

//[.text-justify]
Käesolevas ülesandes on soovitatav ja lubatud LCD ja VL53L0X ToF-sensoriga suhtlemiseks kasutada vabalt valitud teeke.

==== Arvestus

Käesoleva ülesande arvestuse saamiseks tuleb näidata juhendajale:

//[.text-justify]
. programm, mis mõõdab ToF-sensoriga perioodiliselt kaugust ja kuvab selle LCD-ekraanil koos mõõtühikuga:
.. ekraan ei tohi vilkuda;
.. mõõtmised peavad olema selliselt keskmistatud, et väljund oleks stabiilne;
.. suunata DS323M-ist loetavad temperatuuriväärtused __Serial Monitori__ asemel LCD-le.

=== Ülesanne: OLED-ekraan

==== Kirjeldus

//[.text-justify]
Käesoleva ülesande korral lisada seadmed kahe eelmise ülesande seadistusele, et kõik seadmed töötaks üheaegselt koos.

//[.text-justify]
Käesolevas ülesandes on soovitatav OLED-ekraaniga suhtlemiseks kasutada vabalt valitud teeki.

==== Arvestus

Käesoleva ülesande arvestuse saamiseks tuleb näidata juhendajale:

//[.text-justify]
. programm, mis loeb sisendi __Serial Monitorist__ ja kuvab saadud sõnumi OLED ekraanil;
. programm peab oskama kuvada mitmerealisi sõnumeid selliselt, et reavahetus tehakse tühiku kohal (__word wrap__).

