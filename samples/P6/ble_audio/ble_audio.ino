/*
  Streaming Music from Bluetooth

  Copyright (C) 2020 Phil Schatzmann
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

#include "BluetoothA2DPSink.h"
BluetoothA2DPSink a2dp_sink;

#define ENCA 34
#define ENCB 39
#define BTN 36

bool enca_state, encb_state, encbtn_state;

void avrc_metadata_callback(uint8_t id, const uint8_t *text) {
  Serial.printf("==> AVRC metadata rsp: attribute id 0x%x, %s\n", id, text);
  if (id == 0x01) {
    lcd.setCursor(0, 1);
    lcd.printf("%-16.*s", 16, (char*)text);
  }
}

// for esp_a2d_connection_state_t see https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/bluetooth/esp_a2dp.html#_CPPv426esp_a2d_connection_state_t
void connection_state_changed(esp_a2d_connection_state_t state, void *ptr) {
  Serial.println(a2dp_sink.to_str(state));
  lcd.setCursor(0, 0);
  lcd.printf("%-16.*s", 16, a2dp_sink.to_str(state));
}

// for esp_a2d_audio_state_t see https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/bluetooth/esp_a2dp.html#_CPPv421esp_a2d_audio_state_t
void audio_state_changed(esp_a2d_audio_state_t state, void *ptr) {
  Serial.println(a2dp_sink.to_str(state));
  lcd.setCursor(0, 0);
  lcd.printf("%-16.*s", 16, a2dp_sink.to_str(state));
}

uint8_t amount = 10;
void increase_volume() {
  //if (a2dp_sink.get_audio_state() == ESP_A2D_AUDIO_STATE_STARTED) {
    uint8_t vol = a2dp_sink.get_volume();
    if (vol <= 255-amount) vol+=amount;
    else vol=255;
    a2dp_sink.set_volume(vol);
    Serial.printf("Volume: %d", vol);
  //}
}

void decrease_volume() {
  //if (a2dp_sink.get_audio_state() == ESP_A2D_AUDIO_STATE_STARTED) {
    uint8_t vol = a2dp_sink.get_volume();
    if (vol >= amount) vol-=amount;
    else vol=0;
    a2dp_sink.set_volume(vol);
    Serial.printf("Volume: %d", vol);
  //}
}

bool is_active = true;
void play_start_stop() {
  //if (a2dp_sink.get_audio_state() == ESP_A2D_AUDIO_STATE_STARTED) {
      is_active = !is_active;
      if (is_active) {
        Serial.println("play");
        a2dp_sink.play();
      } else {
        Serial.println("pause");
        a2dp_sink.pause();
      }
  //}
}

void setup() {
  Serial.begin(115200);

  lcd.init();                      // initialize the lcd
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();

  const i2s_config_t i2s_config = {
    .mode = (i2s_mode_t) (I2S_MODE_MASTER | I2S_MODE_TX | I2S_MODE_DAC_BUILT_IN),
    .sample_rate = 44100, // corrected by info from bluetooth
    .bits_per_sample = (i2s_bits_per_sample_t) 16, /* the DAC module will only take the 8bits from MSB */
    .channel_format =  I2S_CHANNEL_FMT_RIGHT_LEFT,
    .communication_format = (i2s_comm_format_t)I2S_COMM_FORMAT_STAND_MSB,
    .intr_alloc_flags = 0, // default interrupt priority
    .dma_buf_count = 8,
    .dma_buf_len = 64,
    .use_apll = false
  };

  a2dp_sink.set_i2s_config(i2s_config);

  a2dp_sink.set_on_connection_state_changed(connection_state_changed);
  a2dp_sink.set_on_audio_state_changed(audio_state_changed);

  a2dp_sink.set_avrc_metadata_callback(avrc_metadata_callback);
  a2dp_sink.start("unts");

  pinMode(ENCA, INPUT);
  pinMode(ENCB, INPUT);
  pinMode(BTN, INPUT);

  //attachInterrupt(digitalPinToInterrupt(ENCA), increase_volume, CHANGE);
  //attachInterrupt(digitalPinToInterrupt(ENCB), decrease_volume, CHANGE);
  //attachInterrupt(digitalPinToInterrupt(BTN), play_start_stop, FALLING);

  enca_state = digitalRead(ENCA);
  encb_state = digitalRead(ENCB);
  encbtn_state = digitalRead(BTN);
}

void loop() {
    bool enca = digitalRead(ENCA);
    bool encb = digitalRead(ENCB);
    bool encbtn = digitalRead(BTN);

    Serial.printf("%d %d %d %d\n", enca, encb, encbtn, analogRead(15));
    
    if (a2dp_sink.get_audio_state() == ESP_A2D_AUDIO_STATE_STARTED) {
      if (encbtn == 1 && encbtn_state == 0){
        play_start_stop();
      }
    }

    if(digitalRead(ENCA)!=digitalRead(ENCB)){
        if (enca != enca_state){
          increase_volume();
        }
        if (encb != encb_state){
          decrease_volume();
        }
      }

    enca_state = enca;
    encb_state = encb;
    encbtn_state = encbtn;
    //delay(50);
}
