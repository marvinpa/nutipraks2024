/*
   Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleScan.cpp
   Ported to Arduino ESP32 by Evandro Copercini
   Changed to a beacon scanner to report iBeacon, EddystoneURL and EddystoneTLM beacons by beegee-tokyo
*/

#include <Arduino.h>

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <BLEEddystoneURL.h>
#include <BLEEddystoneTLM.h>
#include <BLEBeacon.h>

int scanTime = 5;  //In seconds
BLEScan *pBLEScan;

class MyAdvertisedDeviceCallbacks : public BLEAdvertisedDeviceCallbacks {
  void onResult(BLEAdvertisedDevice advertisedDevice) {
    uint8_t *payLoad = advertisedDevice.getPayload();
    // search for Eddystone Service Data in the advertising payload
    // *payload shall point to eddystone data or to its end when not found
    const uint8_t serviceDataEddystone[3] = { 0x16, 0xAA, 0xFE };  // it has Eddystone BLE UUID
    const size_t payLoadLen = advertisedDevice.getPayloadLength();
    uint8_t *payLoadEnd = payLoad + payLoadLen - 1;  // address of the end of payLoad space
    while (payLoad < payLoadEnd) {
      if (payLoad[1] == serviceDataEddystone[0] && payLoad[2] == serviceDataEddystone[1] && payLoad[3] == serviceDataEddystone[2]) {
        // found!
        payLoad += 4;
        break;
      }
      payLoad += *payLoad + 1;  // payLoad[0] has the field Length
    }

    if (payLoad < payLoadEnd)  // Eddystone Service Data and respective BLE UUID were found
    {
      if (*payLoad == 0x20) {
        Serial.println("Found an EddystoneTLM beacon!");

        float rssi = (float)advertisedDevice.getRSSI();
        float distance = pow(10, ((-50 - rssi) / 20));
        Serial.printf("RSSI: %.0f dB\n", rssi);
        Serial.printf("Distance: %.2f m\n", distance);

        BLEEddystoneTLM eddystoneTLM;
        eddystoneTLM.setData(std::string((char *)payLoad, 14));
        Serial.printf("Reported battery voltage: %dmV\n", eddystoneTLM.getVolt());
        Serial.printf("Reported temperature: %.2f°C (raw data=0x%04X)\n", eddystoneTLM.getTemp(), eddystoneTLM.getRawTemp());
        Serial.printf("Reported advertise count: %d\n", eddystoneTLM.getCount());
        Serial.printf("Reported time since last reboot: %ds\n", eddystoneTLM.getTime());
        Serial.println("\n");
        
        if (advertisedDevice.haveName()) {
          Serial.printf("Device name: %s\n", advertisedDevice.getName().c_str());
          if (advertisedDevice.getName().compare("NutiTest") == 0){
              lcd.setCursor(0, 0);
              lcd.printf("%dmV %.2fC", eddystoneTLM.getVolt(), eddystoneTLM.getTemp());
              lcd.setCursor(0, 1);
              lcd.printf("%.2fm", distance);
          }          
        }

        if (advertisedDevice.haveServiceUUID()) {
          BLEUUID devUUID = advertisedDevice.getServiceUUID();
          Serial.printf("Found ServiceUUID: %s\n", devUUID.toString().c_str());
        }
      }
    }
  }
};

void setup() {
  lcd.init();                      // initialize the lcd
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  
  Serial.begin(115200);
  Serial.println("Scanning...");

  BLEDevice::init("");
  pBLEScan = BLEDevice::getScan();  //create new scan
  pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
  pBLEScan->setActiveScan(true);  //active scan uses more power, but get results faster
  pBLEScan->setInterval(100);
  pBLEScan->setWindow(99);  // less or equal setInterval value
}

void loop() {
  // put your main code here, to run repeatedly:
  BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
  Serial.print("Devices found: ");
  Serial.println(foundDevices.getCount());
  Serial.println("Scan done!");
  pBLEScan->clearResults();  // delete results fromBLEScan buffer to release memory
  delay(2000);
}