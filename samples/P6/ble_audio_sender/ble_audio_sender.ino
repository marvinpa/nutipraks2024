#include "BluetoothA2DPSource.h"
#include <SPI.h>
#include <mySD.h>

typedef struct __attribute__ ((__packed__)) wav_header {
    // RIFF Header
    char riff_header[4]; // Contains "RIFF"
    uint32_t wav_size; // Size of the wav portion of the file, which follows the first 8 bytes. File size - 8
    char wave_header[4]; // Contains "WAVE"
    
    // Format Header
    char fmt_header[4]; // Contains "fmt " (includes trailing space)
    uint32_t fmt_chunk_size; // Should be 16 for PCM
    uint16_t audio_format; // Should be 1 for PCM. 3 for IEEE Float
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate; // Number of bytes per second. sample_rate * num_channels * Bytes Per Sample
    uint16_t sample_alignment; // num_channels * Bytes Per Sample
    uint16_t bit_depth; // Number of bits per sample
    
    // Data
    char data_header[4]; // Contains "data"
    uint32_t data_bytes; // Number of bytes in data. Number of samples * num_channels * sample byte size
    // uint8_t bytes[]; // Remainder of wave file is bytes
} wav_header_t;

void print_wav_header(wav_header_t *h){
  Serial.println("WAV header");
  Serial.println();
  Serial.printf("riff header: %.4s\n", h->riff_header);
  Serial.printf("wav size: %ld bytes\n", h->wav_size);
  Serial.printf("wave header: %.4s\n", h->wave_header);
  Serial.printf("fmt header: %.4s\n", h->fmt_header);
  Serial.printf("fmt chunk size: %ld bytes\n", h->fmt_chunk_size);

  Serial.print("audio format: ");
  switch (h->audio_format){
    case 1:
      Serial.print("PCM");
      break;
    case 3:
      Serial.print("IEEE float");
      break;
    default:
      Serial.print("Unknown");
      break;
  }
  Serial.printf(" (%d)\n", h->audio_format);

  Serial.printf("channels: %d\n", h->num_channels);
  Serial.printf("sample rate: %ld\n", h->sample_rate);
  Serial.printf("byte rate: %ld\n", h->byte_rate);
  Serial.printf("sample alignment: %d\n", h->sample_alignment);
  Serial.printf("bit depth: %d\n", h->bit_depth);
  Serial.printf("data header: %.4s\n", h->data_header);
  Serial.printf("data size: %ld bytes\n", h->data_bytes);

  Serial.println(); 
}

#define SD_CS_PIN        12
#define SD_CLK_PIN       18
#define SD_MOSI_PIN      23
#define SD_MISO_PIN      19

BluetoothA2DPSource a2dp_source;
ext::File sound_file;
const char* file_name = "/o.wav";
const int sd_ss_pin = 12;

// callback used by A2DP to provide the sound data
int32_t get_sound_data(Frame *data, int32_t len) {
  // the data in the file must be in int16 with 2 channels 
  size_t result_len_bytes = sound_file.read((uint8_t*)data, len * sizeof(Frame) );
  delay(1);

  return result_len_bytes/sizeof(Frame);
}

// Arduino Setup
void setup(void) {
  Serial.begin(115200);

  pinMode(SD_CS_PIN, OUTPUT); // SS
  //SPIClass spiSD = SPIClass(HSPI); // Neither HSPI nor VSPI seem to work
  //spiSD.begin(SD_CLK_PIN, SD_MISO_PIN, SD_MOSI_PIN, SD_CS_PIN); 
  // Setup SD and open file
  if (!SD.begin(SD_CS_PIN, SD_MOSI_PIN, SD_MISO_PIN, SD_CLK_PIN))//, 1 * MHz)) - no freq work: 1, 25, 40, 50, 80 MHz
      while (1){
        Serial.println("SD Initialization failed!");
        delay(1000);
      }
  sound_file = SD.open(file_name, FILE_READ);
  if (!sound_file){
    while (1){
      Serial.println("Failed to open file");
      delay(1000);
    };
  }

  wav_header_t sound_header; 
  sound_file.read((uint8_t *)&sound_header, 44);
  print_wav_header(&sound_header);

  // start the bluetooth
  Serial.println("starting A2DP...");

  //a2dp_source.set_auto_reconnect(false);
  a2dp_source.start("unts", &get_sound_data);  
}

// Arduino loop - repeated processing 
void loop() {

} 