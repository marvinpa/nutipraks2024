#include "Wire.h"
#include <Arduino.h>

#include "BLEDevice.h"
#include "BLEUtils.h"
#include "BLEBeacon.h"
#include "BLEAdvertising.h"
#include "BLEEddystoneTLM.h"

const uint16_t beaconUUID = 0xFEAA;

#define DS3231M_TEMPERATURE 0x11
#define DS3231M_ADDRESS 0x68

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
BLEAdvertising *pAdvertising;

#define BEACON_UUID "8ec76ea3-6668-48da-9866-75be8bc86f4d" // UUID 1 128-Bit (may use linux tool uuidgen or random numbers via https://www.uuidgenerator.net/)

//Eddystone TLM packet specification: https://github.com/google/eddystone/blob/master/eddystone-tlm/tlm-plain.md
std::string generateBeaconData(float temp, uint16_t voltage, uint32_t count, uint32_t time){
  uint16_t fp_temp = (uint16_t)(round(temp * (1 << 8)));
  char beacon_data[14];
  beacon_data[0] = 0x20;                          // Eddystone Frame Type (Unencrypted Eddystone-TLM)
  beacon_data[1] = 0x00;                          // TLM version
  beacon_data[2] = ((voltage & 0xFF00) >> 8);     // Battery voltage, 1 mV/bit i.e. 0xCE4 = 3300mV = 3.3V
  beacon_data[3] = (voltage & 0xFF);              //
  beacon_data[4] = ((fp_temp & 0xFF00) >> 8);     // Beacon temperature Digit
  beacon_data[5] = (fp_temp & 0xFF);              // Decimal
  beacon_data[6] = ((count & 0xFF000000) >> 24);  // Advertising PDU count
  beacon_data[7] = ((count & 0xFF0000) >> 16);    //
  beacon_data[8] = ((count & 0xFF00) >> 8);       //
  beacon_data[9] = (count & 0xFF);                //
  beacon_data[10] = ((time & 0xFF000000) >> 24);  // Time since power-on or reboot as 0.1 second resolution counter
  beacon_data[11] = ((time & 0xFF0000) >> 16);    //
  beacon_data[12] = ((time & 0xFF00) >> 8);       //
  beacon_data[13] = (time & 0xFF);                //
  return std::string(beacon_data, 14);
}

void setup()
{
  Wire.begin();
  Serial.begin(115200);

  // Create the BLE Device
  BLEDevice::init("NutiTest");
  BLEDevice::setPower(ESP_PWR_LVL_N12);

  //beacon name advertizement
  pAdvertising = BLEDevice::getAdvertising();
  BLEAdvertisementData oAdvertisementData = BLEAdvertisementData();
  oAdvertisementData.setName("NutiTest");
  pAdvertising->setAdvertisementData(oAdvertisementData);

}

int counter = 0;
void loop()
{
  uint16_t volt = analogRead(15)*0.80586080586;

  //Write message to the slave
  Wire.beginTransmission(DS3231M_ADDRESS);  // Address the I2C device
  Wire.write(DS3231M_TEMPERATURE);          // Send the register address to read
  Wire.endTransmission();                   // Close transmission
  Wire.requestFrom(DS3231M_ADDRESS, 1);     // Request 1 byte of data
  float temp = (int)Wire.read();                // MSB

  Wire.write(DS3231M_TEMPERATURE + 1);      // Send the register address to read
  Wire.endTransmission();                   // Close transmission
  Wire.requestFrom(DS3231M_ADDRESS, 1);     // Request 1 byte of data
  temp += (float)(Wire.read()>>6)*0.25;        //LSB

  uint32_t boot_time = millis()/100;
  counter++;
  
  Serial.printf("%dmV, %fC, %d, %d\n", volt, temp, counter, boot_time);

  BLEAdvertisementData oScanResponseData;
  oScanResponseData.setFlags(0x06); // GENERAL_DISC_MODE 0x02 | BR_EDR_NOT_SUPPORTED 0x04
  oScanResponseData.setCompleteServices(BLEUUID(beaconUUID));
  oScanResponseData.setServiceData(
    BLEUUID(beaconUUID), 
    generateBeaconData(temp, volt, counter, boot_time)
  );
  
  pAdvertising->stop();
  pAdvertising->setScanResponseData(oScanResponseData);
  pAdvertising->start();
  delay(1000);
}
